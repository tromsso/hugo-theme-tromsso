const path = require('path');

module.exports = {
  entry: './assets/ts/contact_form_firebase.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    modules: [
      path.resolve('./node_modules')
    ]
  },
  output: {
    filename: 'contact_form_firebase.js',
    path: path.resolve(__dirname, 'assets/js')
  }
};
