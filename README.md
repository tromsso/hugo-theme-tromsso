# Tromsso

Hugo theme for Tromsso landing page

## Usage

Bulma and Bulma pricing table are packaged as Git submodules for easy cloning and adding to Hugo themes directory. Using Hugo Pipes, the SASS modules are compiled as required into versioned CSS.

## Development

Typescript is used for any Javascript development with webpack to compile and output optimised JS. 

```
$ # Install dependencies
$ yarn

$ # Enable webpack to watch and update files
$ npm start
```