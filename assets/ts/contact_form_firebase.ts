import * as firebase from 'firebase/app'
import 'firebase/firestore'

/// Consts and setup ///
declare const newsletterRef: string
declare const interestRef: string
declare const config: any

firebase.initializeApp(config)

const firestoreSettings = {
  timestampsInSnapshots: true,
}

const db = firebase.firestore()
db.settings(firestoreSettings)

const interestFormElement = document.getElementById('interest_form')
if (interestFormElement) {
  interestFormElement.addEventListener('submit', registerInterest)
}

const newsletterFormElement = document.getElementById('newsletter_form')
if (newsletterFormElement) {
  newsletterFormElement.addEventListener('submit', signupForNewsletter)
}

/// Classes ///
class NewsletterRequest {
  email: String
  constructor() {
    this.email = inputValue('newsletter_email')
  }

  save(): void {
    db.collection(newsletterRef).add({
      email: this.email,
    }).then(() => {
      thankYouModal()
      if (newsletterFormElement) {
        (<HTMLFormElement>newsletterFormElement).reset()
      }
    }).catch((error) => {
      console.log(error)
      thankYouModal()
    })
  }
}

class InterestRequest extends NewsletterRequest {
  name: String
  company: String
  companyLocation: String
  mau: number
  constructor() {
    super()
    this.email = inputValue('interest_email')
    this.name = inputValue('interest_name')
    this.company = inputValue('interest_company')
    this.companyLocation = inputValue('interest_company_location')
    this.mau = Number(inputValue('interest_mau'))
  }

  save(): void {
    db.collection(interestRef).add({
      email: this.email,
      name: this.name,
      company: this.company,
      companyLocation: this.companyLocation,
      mau: this.mau,
    }).then(() => {
      thankYouModal()
      if (interestFormElement) {
        (<HTMLFormElement>interestFormElement).reset()
      }
    }).catch((error) => {
      console.log(error)
      thankYouModal()
    })
  }
}

/// Utility functions ///
function thankYouModal() {
  const modalElement = document.getElementById('modal')
  if (!modalElement) {
    return
  } else {
    modalElement.classList.add('is-active')
  }

  const htmlElement = document.querySelector('html')
  if (htmlElement) {
    htmlElement.classList.add('is-clipped')
  }

  setTimeout(() => {
    modalElement.classList.remove('is-active')
    if (htmlElement) {
      htmlElement.classList.remove('is-clipped')
    }
  }, 3000);
}

function registerInterest(ev: Event) {
  ev.preventDefault()
  let req = new InterestRequest()
  req.save()
}

function signupForNewsletter(ev: Event) {
  ev.preventDefault()
  let req = new NewsletterRequest()
  req.save()
}

function inputValue(selectorID: string) {
  return (<HTMLInputElement>document.getElementById(selectorID)).value
}

