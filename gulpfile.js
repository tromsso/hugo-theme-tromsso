'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
var revall = require("gulp-rev-all");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");

sass.compiler = require('node-sass');

gulp.debug = true;

gulp.task('dev', function () {
  return gulp.src('./assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./static/css'));
});

gulp.task('sass', function () {
  return gulp.src('./assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('dev:watch', function () {
  gulp.watch('./assets/sass/**/*.scss', ['dev']);
});

gulp.task('version', function () {
  gulp.src('./assets/**')
    .pipe(revall.revision({
      dontGlobal: ['/^\/favicon.ico$/', '.html', '.ts', '.scss'],
      debug: true,
    }))
    .pipe(gulp.dest('./static'))
})

gulp.task('default', ['sass', 'clean', 'version'])

gulp.task('watch', function () {
  gulp.watch('./assets/**/*', ['default']);
})

gulp.task('clean', function () {
  return del(['./static/**/*.css', './static/**/*.scss']);
})

gulp.task("ts", function () {
  return tsProject.src()
      .pipe(tsProject())
      .js.pipe(gulp.dest("./static/js/"));
});
